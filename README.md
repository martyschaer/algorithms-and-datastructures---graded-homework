# Algorithms and Datastructures - Graded Homework

The graded homeworks I did for my A&D class with two other students.

This repository just contains links to the projects.

# Links

## Homework 1
**Link:** [AlDa-HW1](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-1)
**Task:** Create different implementations for calculating the fibonacci sequence and benchmark them.

## Homework 2
**Link:** [AlDa-HW2](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-2)
**Task:** Prove the summation laws for arithmetic and geometric series using induction.

## Homework 3
**Link:** [AlDa-HW3](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-3)
**Task:** Prove various propositions surrounding logarithms and other asymptotic complexities.

## Homework 4
**Link:** [AlDa-HW4](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-4)
**Task:** Prove relations between Big-O, Big-Omega, Big-Theta and little-o, and little-omega

## Homework 5
**Link:** [AlDa-HW5](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-5)
**Task:** Create different implementations of mergesort and benchmark them.

## Homework 6
**Link:** [AlDa-HW6](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-6)
**Task:** Create different implementations of quicksort and benchmark them.

## Homework 7
**Link:** [AlDa-HW7](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-7)
**Task:** Explain and implement binary search for different goals and algorithms.